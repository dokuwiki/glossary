<?php
  /**
   * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
   * @author     Francois Merciol <dokuplugin@merciol.fr>
   *
   * Plugin Glossary: manage forms for glossary
   */
 
if (!defined ('DOKU_INC'))
    define ('DOKU_INC', realpath (dirname (__FILE__).'/../../../').'/');
if (!defined ('DOKU_PLUGIN'))
    define ('DOKU_PLUGIN', DOKU_INC.'lib/plugins/');
require_once (DOKU_PLUGIN.'syntax.php');

require_once (dirname (__FILE__).'/../glossary.class.php');

// ============================================================
class syntax_plugin_glossary_span extends DokuWiki_Syntax_Plugin {
 
  // ============================================================
  function getType () { return 'formatting'; }
  function getPType () { return 'normal'; }
  function getAllowedTypes() { return array ('formatting', 'substition', 'disabled'); }
  function getSort () { return 195; }
  function accepts ($mode) {
    if ($mode == substr (get_class ($this), 7))
      return true;
    return parent::accepts ($mode);
  }

  function connectTo ($mode) {
    $this->Lexer->addSpecialPattern ('<glossary[^/]*?/>', $mode, 'plugin_glossary_span');
    $this->Lexer->addEntryPattern ('<glossary[^/]*?>(?=.*?</glossary>)', $mode, 'plugin_glossary_span');
  }
  function postConnect() {
    $this->Lexer->addExitPattern ('</glossary>', 'plugin_glossary_span');
  }

  // ============================================================
  function handle ($match, $state, $pos, Doku_Handler $handler) {
    switch ($state) {
    case DOKU_LEXER_SPECIAL :
      $data = strtolower (trim (substr ($match, 9, -2)));
      return array ($state, $data);
    case DOKU_LEXER_ENTER:
      $data = strtolower (trim (substr ($match, 9, -1)));
      return array ($state, $data);

    case DOKU_LEXER_UNMATCHED :
      $handler->_addCall ('cdata', array ($match), $pos);
      return false;

    case DOKU_LEXER_EXIT :
      return array ($state, '');
    }
    return false;
  }

  // ============================================================
  function render ($mode, Doku_Renderer $renderer, $indata) {
    if (empty ($indata))
      return false;
    if ($mode != 'xhtml')
      false;
    list ($state, $data) = $indata;
    $data = trim ($data);
    switch ($state) {
    case DOKU_LEXER_SPECIAL :
      $imgDir = DOKU_REL.'lib/plugins/glossary/images/';
      if ($data == "help")
	$renderer->doc .= '<img src="'.$imgDir.'help.png" />';
      elseif ($data == "clock")
	$renderer->doc .= '<img src="'.$imgDir.'clock.png" />';
      elseif ($data == "face-smile")
	$renderer->doc .= '<img src="'.$imgDir.'face-smile.png" />';
      elseif ($data == "face-sad")
	$renderer->doc .= '<img src="'.$imgDir.'face-sad.png" />';
      elseif ($data == "stop")
	$renderer->doc .= '<img src="'.$imgDir.'stop.png" />';
      elseif ($data == "one-way")
	$renderer->doc .= '<img src="'.$imgDir.'one-way.png" />';
      elseif ($data == "eye")
	$renderer->doc .= '<img src="'.$imgDir.'eye.png" />';
      elseif ($data == "score")
	$renderer->doc .= '<img src="'.$imgDir.'score-all.png" width="32" />';
     elseif ($data == "search")
	$renderer->doc .= '<img src="'.$imgDir.'search.png" />';
      elseif (preg_match_all ("#(?<ns>.*)\s+(?<type>definition|proposal|poll)#", $data, $dumy) > 0) {
	global $ID;
	$ns = getNS ($ID);
	if (count ($dumy ['ns']) > 0) {
	  $ns = $dumy['ns'][0];
	  if (($ns == '*') || ($ns == ':'))
	    $ns = '';
	  elseif ($ns == '.')
	    $ns = getNS ($ID);
	  else
	    $ns = cleanID ($ns);
	}
	$glossary = new glossary ($this, $ns);
	switch ($dumy['type'][0]) {
	case "definition":
	  $renderer->doc .= $glossary->getDefinitionSize ();
	  break;
	case "proposal":
	  $renderer->doc .= $glossary->getProposalSize ();
	  break;
	case "poll":
	  $renderer->doc .= $glossary->getPollSize ();
	  break;
	}
      }
      break;
    case DOKU_LEXER_ENTER:
      if ($data == "word")
	$renderer->doc .= '<span class="glossaryWord">';
      elseif ($data == "translate")
	$renderer->doc .= '<span class="glossaryTranslate">';
      else
	$renderer->doc .= '<span>';
      break;
    case DOKU_LEXER_EXIT:
      $renderer->doc .= "</span>";
      break;
    }
    return true;
  }

  // ============================================================
} // syntax_plugin_GLOSSARY
?>
