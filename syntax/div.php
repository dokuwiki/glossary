<?php
  /**
   * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
   * @author     Francois Merciol <dokuplugin@merciol.fr>
   *
   * Plugin Glossary: manage forms for glossary
   */

if (!defined ('DOKU_INC'))
    define ('DOKU_INC', realpath (dirname (__FILE__).'/../../../').'/');
if (!defined ('DOKU_PLUGIN'))
    define ('DOKU_PLUGIN', DOKU_INC.'lib/plugins/');
require_once(DOKU_PLUGIN.'syntax.php');

require_once (dirname (__FILE__).'/../glossary.class.php');

// ============================================================
class syntax_plugin_glossary_div extends DokuWiki_Syntax_Plugin {
 
  // ============================================================
  function getType () { return 'substition'; }
  function getPType () { return 'block'; }
  function getAllowedTypes () { return array ('container', 'baseonly', 'formatting', 'substition', 'protected', 'disabled', 'paragraphs'); }
  function getSort () { return 309; }
  function connectTo ($mode) {
    $this->Lexer->addSpecialPattern ('\{\{glossary[^}]*\}\}', $mode, 'plugin_glossary_div');
  }
 
  // ============================================================
  function handle ($match, $state, $pos, Doku_Handler $handler) {
    switch ($state) {
    case DOKU_LEXER_SPECIAL :
      return array ($state, trim (substr ($match, 10, -2))); // "{{glossary" => 10 "}}" => 2
    }
    return false;
  }

  // ============================================================
  function render ($mode, Doku_Renderer $renderer, $indata) {
    if ($mode != 'xhtml')
      return false;
    list ($state, $data) = $indata;
    $data = " ".$data." ";
    $renderer->info ['cache'] = FALSE;
    $cmd = "";
    $arg = "";

    if (preg_match_all ("#(\"[^\"]*\")* help (\"[^\"]*\")*#", strtolower ($data), $dumy) > 0) {
      $this->help ($renderer);
      return true;
    }
    // namespace
    global $ID;
    $ns = getNS ($ID);
    if (preg_match_all ("#>([^ ]*) .*#", strtolower ($data), $dumy) > 0) {
      $ns = $dumy[1][0];
      if (($ns == '*') || ($ns == ':'))
	$ns = '';
      elseif ($ns == '.')
	$ns = getNS ($ID);
      else
	$ns = cleanID ($ns);
    }
    if (preg_match_all ("#(\"[^\"]*\")* sample (\"[^\"]*\")*#", strtolower ($data), $dumy) > 0)
      $cmd = "printSample";
    elseif (preg_match_all ("#(\"[^\"]*\")* list (\"[^\"]*\")*#", strtolower ($data), $dumy) > 0)
      $cmd = "printList";
    elseif (preg_match_all ("#(\"[^\"]*\")* poll=\"(?<word>[^\"]*)\" (\"[^\"]*\")*#", $data, $dumy) > 0) {
      $cmd = "printPoll";
      $arg = $dumy['word'][0];
    } elseif (preg_match_all ("#(\"[^\"]*\")* proposal (\"[^\"]*\")*#", strtolower ($data), $dumy) > 0)
      $cmd = "printProposal";
    elseif (preg_match_all ("#(\"[^\"]*\")* admin-proposal (\"[^\"]*\")*#", strtolower ($data), $dumy) > 0)
      $cmd = "adminProposal";
    elseif (preg_match_all ("#(\"[^\"]*\")* admin-definition (\"[^\"]*\")*#", strtolower ($data), $dumy) > 0)
      $cmd = "adminDefinition";
    elseif (preg_match_all ("#(\"[^\"]*\")* admin-glossaries (\"[^\"]*\")*#", strtolower ($data), $dumy) > 0)
      $cmd = "adminGlossaries";
    else {
      $this->help ($renderer);
      return true;
    }

    $glossary = new glossary ($this, $ns);
    ob_start ();
    $glossary->$cmd ($arg);
    $text = ob_get_contents();
    ob_end_clean ();
    foreach ($glossary->message as $type => $msg)
      $text = '<div class="'.$type.'">'.$msg.'</div>'.$text;
    $renderer->doc .= '<div class="glossary">'.$text.'</div>';
    return true;
  }

  // ============================================================
  function isAdmin () {
    global $INFO;
    return
      isset ($INFO ['userinfo']) &&
      isset ($INFO ['userinfo']['grps']) &&
      in_array (trim ($this->getConf ('adminGroup')), $INFO ['userinfo']['grps']);
  }

  // ============================================================
  function help (Doku_Renderer $renderer) {
    $url = "http://admin.parlenet.org/plugins/glossary/";
    $renderer->doc .=
      '      <h1>Help Glossary</h1>'.NL.
      '      <ul>'.NL.
      '        <li><b>{{glossary&gt;</b>namespace [help] (sample|list|proposal|admin-proposal|admin-definition|admin-glossaries)<b>}}</b></li>'.NL.
      '        <li><b>&lt;glossary</b> (help|clock|face-smile|face-sad|stop|one-way|search) <b>/&gt;</b></li>'.NL.
      '        <li><b>&lt;glossary</b> (word|translate) <b>&gt;</b> ... <b>&lt;/glossary&gt;</b></li>'.NL.
      '      </ul>'.NL.
      '      <p><a class="urlextern" rel="nofollow" title="'.$url.'" href="'.$url.'">'.$url.'</a></p>'.NL;
  }

  // ============================================================
} // syntax_plugin_GLOSSARY
?>
