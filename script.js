/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * Javascript functionality for the glossary plugin
 */

// ========================================
// Util
// ========================================
function trim (str) {
    return str.replace(/^\s+/g,'').replace(/\s+$/g,'') ;
}

function glossaryClearMsgJ (item) {
    // efface le popup
    item.closest ("div").find ("div.popup").each (function () {
	jQuery (this).remove ();
   });
}

function glossaryZebraJ (jTable) {
    // alterne les couleurs des lignes du tableau
    var oddEven = new Array ("odd", "even");
    var even = 0;
    jTable.find ("tr").each (function () {
	var item = jQuery (this);
	if (item.hasClass ("title"))
	    return;
	if (item.css ("display") == "none")
	    return;
	this.className = oddEven [even];
	even = 1 - even;
    });
}

// ========================================
// Clear form
// ========================================
function glossaryReset (input) {

    // met à blanc le formulaire
    var jForm = jQuery (input).closest ("form");
    jForm.find ("input[type=text]").each (function () {
	this.defaultValue = "";
	jQuery (this).val ("");
    });
    jForm.find ("input[type=textarea]").each (function () {
	this.defaultValue = "";
	jQuery (this).val ("");
    });
    glossaryClearMsgJ (jForm);
}

function glossaryUpdateProposalLabel (form) {
    form.elements[4].value = LANG["plugins"]["glossary"][form.elements[7].value ? "update" : "proposal"];
}

// ========================================
// Glossary Util
// ========================================
function glossaryComparatorWord (a, b) {
    return glossaryComparator (a, b, "word", false);
}

function glossaryComparatorTranslate (a, b) {
    return glossaryComparator (a, b, "translate", false);
}

function glossaryComparatorDate (a, b) {
    return -glossaryComparator (a, b, "date", false);
}

function glossaryComparatorView (a, b) {
    return -glossaryComparator (a, b, "view", true);
}

function glossaryComparatorScore (a, b) {
    return -glossaryComparator (a, b, "score", false);
}

function glossaryComparator (a, b, name, num) {
    var valA = "";
    for (var i = 0; i < a.attributes.length; i++)
	if (a.attributes [i].name == name) {
	    valA = a.attributes [i].nodeValue;
	    break;
	}
    var valB = "";
    for (var i = 0; i < b.attributes.length; i++)
	if (b.attributes [i].name == name) {
	    valB = b.attributes [i].nodeValue;
	    break;
	}
    if (num) {
	if (!valA)
	    valA = 0;
	if (!valB)
	    valB = 0;
	return parseInt (valA) - parseInt (valB);
    }
    if (valB == valA)
	return 0;
    if (valB > valA)
	return -1;
    return 1;
}

function glossaryGetRowsJ (jTable) {
    // prend les lignes du tableau
    var result = new Array ();
    jTable.find ("tr").each (function () {
	if (jQuery (this).hasClass ("title"))
	    return;
	result.push (this);
    });
    return result;
}

function glossarySetRowsJ (jTable, rows) {
    // fixe les lignes du tableau
    var titles = new Array ();
    jTable.children ().not ("tr").each (function () {
	titles.push (this);
    });
    jTable.children (".title").each (function () {
	titles.push (this);
    });
    jTable.empty ();
    for (var i in titles)
	jTable.append (titles [i]);
    for (var i in rows)
	jTable.append (rows [i]);
}

function glossarySort (input, comparator) {
    // trie le tableau
    var jTable = jQuery (input).closest ("tr").parent ();
    var rows = glossaryGetRowsJ (jTable);
    if (rows.length < 2)
	return;
    rows.sort (comparator);
    glossarySetRowsJ (jTable, rows);
    glossaryZebraJ (jTable);
}

// ========================================
// focus table
// ========================================
function glossarySearch (input) {
    // filtre les lignes du tableau
    var jInput = jQuery (input);
    glossaryClearMsgJ (jInput);
    var value = trim (jInput.val ()).toLowerCase ();
    var jTable = jInput.closest ("tr").parent ();
    jTable.find ("tr").each (function () {
	var item = jQuery (this);
	if (item.hasClass ("title"))
	    return;
	item.css ("display", "");
	if (!value)
	    return;
	var hidden = true;
	item.find ("td.word,td.translate").each (function () {
	    if (this.textContent && this.textContent.toLowerCase ().indexOf (value) >= 0)
		hidden = false;
	});
	if (hidden)
	    item.css ("display", "none");
    });
    glossaryZebraJ (jTable);
    return false;
}

// ========================================
// sending form
// ========================================
function glossaryPoll (ancor, ticket, opinion, NS) {
    // vote
    var params = "glossary[operation]=poll&glossary[ticket]="+ticket+"&glossary[opinion]="+opinion+"&glossary[ns]="+NS;
    glossarySend (ancor, DOKU_BASE+"lib/plugins/glossary/ajax.php", params);
}

function glossaryAjax (form) {
    var params = "";
    for (var idx = 0; idx < form.elements.length; idx++) {
	var elem = form.elements[idx];
	if (elem.type == "checkbox") {
	    if (elem.checked)
		params += "&"+elem.name+"="+elem.value;
	} else
	    params += "&"+elem.name+"="+elem.value;
    }
    glossarySend (form, DOKU_BASE+"lib/plugins/glossary/ajax.php", params);
    return false;
}
function glossarySendClear (action, ns) {
    jQuery.ajax ({
	type: "POST",
	url:  DOKU_BASE+"lib/plugins/glossary/ajax.php",
	cache: false,
	async: true,
	data: "glossary[action]="+action+"&glossary[ns]="+ns,
    });
}

// ========================================
// Ajax function
// ========================================
function glossarySend (sender, uri, params) {
    var jDiv = jQuery (sender).closest ("div");
    glossaryClearMsgJ (jQuery (sender));
    jQuery ('<div class="popup">'+LANG["plugins"]["glossary"]["pleaseWait"]+'</div>').
	insertBefore (jDiv.children ().first ());
    jQuery.ajax ({
	type: "POST",
	url: uri,
	cache: false,
	async: true,
	data: params,
	success: function (response) {
	    jDiv.html (trim (response));
	}
    });
}

// ========================================
