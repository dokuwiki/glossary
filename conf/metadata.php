<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * Metadata for configuration manager plugin
 * Additions for the Glossary plugin
 */
$meta['dataDir']		= array('string');
$meta['recentDays']		= array('numeric');
$meta['maxIP']		  	= array('numeric');
$meta['adminGroup']		= array('string');
$meta['propGroup']		= array('string');
$meta['transSep']		= array('string');
$meta['propositionPage']	= array('string');
$meta['sampleDelai']		= array('numeric');
$meta['listDelai']		= array('numeric');
?>
