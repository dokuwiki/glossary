<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * Options for the Glossary Plugin
 */
$conf['dataDir']		= 'glossary';
$conf['recentDays']		= 7;
$conf['maxIP']			= 5;
$conf['adminGroup']		= 'admin';
$conf['propGroup']		= 'glossary';
$conf['transSep']		= ';';
$conf['propositionPage']	= '[[.:proposition|proposition]]';
$conf['sampleDelai']		= 2*60*60;
$conf['listDelai']		= 2*60*60;
?>
