<?php
  /**
   * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
   * @author     Francois Merciol <dokuplugin@merciol.fr>
   *
   * Plugin Glossary
   */
if (!defined ('DOKU_INC'))
  define ('DOKU_INC', realpath (dirname (__FILE__).'/../../../').'/');
require_once (DOKU_INC.'inc/init.php');
require_once (DOKU_INC.'inc/common.php');
require_once (DOKU_INC.'inc/auth.php');
if (isset ($_SERVER['REMOTE_USER']))
  $INFO['userinfo'] = $auth->getUserData ($auth->cleanUser ($_SERVER['REMOTE_USER']));
{
  $glossaryPlugin =& plugin_load ('syntax', 'glossary_div');
  $glossary = new glossary ($glossaryPlugin, $_REQUEST ['glossary']['ns']);

  $cacheCmd = true;
  switch ($_REQUEST ['glossary']['action']) {
  case 'clear':
    $glossary->clearCache (md5 ($_REQUEST ['glossary']['ns']));
    break;
  case 'clearAll':
    $glossary->clearCache (null);
    break;
  default:
    $cacheCmd = false;
  }
  if (!$cacheCmd) {
    ob_start ();
    switch ($_REQUEST ['glossary']['operation']) {
    case 'poll':
      $glossary->poll ();
      break;
    case 'record':
      $glossary->printProposal ();
      break;
    case $glossary->prop.'remove':
      $glossary->adminProposal ();
      break;
    case $glossary->def.'remove':
    case $glossary->def.'update':
      $glossary->adminDefinition ();
    case 'glos-remove':
      $glossary->adminGlossaries ();
      break;
    }
    $text = ob_get_contents ();
    ob_end_clean ();
    foreach ($glossary->message as $type => $msg)
      $text = '<div class="'.$type.'">'.$msg.'</div>'.$text;
    ptln ($text);
  }
}
?>
