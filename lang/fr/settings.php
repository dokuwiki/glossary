<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * French language file
 */

// for the configuration manager
$lang['dataDir']		= 'répertoire où sont placer les données';
$lang['recentDays']		= 'nombre de jours pendant lesquels la définition est considéré comme récente';
$lang['adminGroup']		= 'groupe de gestion des propositions et des définitions';
$lang['maxIP']			= 'nombre maximum de proposition en attente par adresse IP';
$lang['propGroup']		= 'groupe sans limite de proposition';
$lang['transSep']		= 'spérateur de qui implique un retour à la ligne dans la liste des traductions';
$lang['propositionPage']	= 'page où se trouve le formulaire de proposition (acceuil du glossaire)';
$lang['sampleDelai']		= 'Temps de cache pour l\'exemple (en secondes)';
$lang['listDelai']		= 'Temps de cache pour la liste (en secondes)';
?>
