<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * French language file
 */

// javascript
$lang['js'] =
    array ('pleaseWait' => 'Connexion avec le serveur en cours ...',
	   'proposal' => 'Faire un don',
	   'update' => 'Mise à jour');

// commands
$lang['remove']			= 'Supprime';
$lang['update']			= 'Mise à jour';
$lang['proposal']		= 'Faire un don';
$lang['new']			= 'Recommencer';
$lang['clear']			= 'Effacer le cache';
$lang['clearAll']		= 'Effacer tous les caches';

// labels
$lang['ticketIfUpdate']		= 'Ticket (si modification) :';
$lang['ticket']			= 'Ticket';
$lang['useTicket']		= 'Utilise ticket';
$lang['date']			= 'Date';
$lang['ip']			= 'IP';
$lang['email']			= 'Mel';
$lang['word']			= 'Ne dites plus !';
$lang['translate']		= 'Dites !';
$lang['why']			= '?';
$lang['poll']			= 'Avis';
$lang['createPage']		= 'Création de page';

// messages
$lang['readData']		= 'Relecture des données enregistrées.';
$lang['wordMandatory']		= '"Ne dites plus !" nécessaire.';
$lang['translateMandatory']	= '"Dites !" nécessaire.';
$lang['proposalRecorded']	= 'Votre don est bien enregistré (donnez nous du temps pour l\'étudier).<br/>'.
  'Il faut cliquer sur "Recommencer" pour un nouveau ticket et ne pas perdre ce don.<br/>'.
  'Pour modifier votre demande, voici votre ticket : ';
$lang['ticketDeleted']		= ' supprimé.';
$lang['pageDeleted']		= 'Page supprimée.';
$lang['lastIP']			= 'C\'est votre dernier don. Il faut nous laisser le temps de les étudier :-).';
$lang['maxIP']			= 'Vous avez fait beaucoup de dons. Laissez-nous le temps de les étudier :-).';
$lang['noDef']			= 'La définition n\'éxiste plus.';
$lang['writePoll']		= 'Avis enregistré.';
$lang['glossaryNotEmpty']	= 'Impossible de supprimer le glossaire non vide : ';
$lang['glossaryRemoved']	= 'Glossaire supprimé : ';
$lang['notPolledYet']		= 'Soyez le 1er à donner votre avis !';
$lang['notifySubject']		= '[glossaire] Une nouvelle proposition !';
$lang['notifyContent']		= "Une nouvelle proposition :\n\nNe dites plus : @WORD@\nDites :         @TRANSLATE@\n\n";
$lang['badCaptcha']		= "Etes-vous vraiment un humain ?";

// toolTip
$lang['tipWord']		= 'Tri par "Ne dites plus !"';
$lang['tipTranslate']		= 'Tri par "Dites !"';
$lang['tipDate']		= 'Tri par dates';
$lang['tipView']		= 'Tri par vues';
$lang['tipScore']		= 'Tri par avis';
$lang['tipSearch']		= 'Recherche le texte';
$lang['tipWhy']			= 'Un simple clic et<br/>l\'explication déchire !';
$lang['tipPoll']		= 'Un clic pour :<br/> voir l\'explication,<br/> et donner un avis.';
$lang['tipPollDown']		= 'J\'aime pas !<br/>Juste un clic pour le dire.';
$lang['tipPollUp']		= 'J\'aime !<br/>Juste un clic pour le dire.';
?>
