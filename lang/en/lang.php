<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * English language file
 */

// javascript
$lang['js'] =
    array ('pleaseWait' => 'Connection to the server in progress ...',
	   'proposal' => 'Proposal',
	   'update' => 'Update');

// commands
$lang['remove']			= 'Remove';
$lang['update']			= 'Update';
$lang['proposal']		= 'Proposal';
$lang['new']			= 'Reset';
$lang['clear']			= 'Clear cache';
$lang['clearAll']		= 'Clear all cache';

// labels
$lang['ticketIfUpdate']		= 'Ticket (if update) :';
$lang['ticket']			= 'Ticket';
$lang['useTicket']		= 'Use ticket';
$lang['date']			= 'Date';
$lang['ip']			= 'IP';
$lang['email']			= 'e-mail';
$lang['word']			= 'Word';
$lang['translate']		= 'Translate';
$lang['why']			= 'Why ?';
$lang['poll']			= 'Poll ?';
$lang['why']			= 'Why';


// messages
$lang['readData']		= 'Reread recording data.';
$lang['wordMandatory']		= '"Word" mandatory.';
$lang['translateMandatory']	= '"translate" mandatory.';
$lang['proposalRecorded']	= 'Your proposal is actually recorded or updated with the ticket : ';
$lang['ticketDeleted']		= ' removed.';
$lang['pageDeleted']		= 'Page removed.';
$lang['lastIP']			= 'It is your last proposal. We need time to study them.';
$lang['maxIP']			= 'You send many proposals. Let us time to study them :-).';
$lang['noDef']			= 'Definition doesn\'t exist anymore.';
$lang['writePoll']		= 'Poll recorded.';
$lang['glossaryNotEmpty']	= 'Can\'t remove the no-empty glossary: ';
$lang['glossaryRemoved']	= 'Glossary removed : ';
$lang['notPolledYet']		= 'Be the first to poll!';
$lang['notifySubject']		= '[glossary] New proposal!';
$lang['notifyContent']		= "New proposal:\n\nWord:      @WORD@\nTranslate: @TRANSLATE@\n\n";
$lang['badCaptcha']		= "Are you actually a human ?";
$lang['createPage']		= 'Create page';

// toolTip
$lang['tipWord']		= 'Sort by "Word';
$lang['tipTranslate']		= 'Sort by "Translate"';
$lang['tipDate']		= 'Sort by creation';
$lang['tipView']		= 'Sort by view';
$lang['tipScore']		= 'Sort by score';
$lang['tipSearch']		= 'Search text';
$lang['tipWhy']			= 'Click! See the amazing explanation!';
$lang['tipPoll']		= 'Clik to see the explanation and poll.';
$lang['tipPollDown']		= 'I poll against!';
$lang['tipPollUp']		= 'I poll for!';
?>
