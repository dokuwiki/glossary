<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * English language file
 */
 
// for the configuration manager
$lang['dataDir']		= 'directory where data are placed';
$lang['recentDays']		= 'number of days the definition is considared new';
$lang['adminGroup']		= 'group to managed proposition and definition';
$lang['maxIP']			= 'maximum waitting proposition per IP adresse';
$lang['propGroup']		= 'group without limite of proposition';
$lang['transSep']		= 'separator that cause a return in translate list';
$lang['propositionPage']	= 'page include proposition form (main glossary page)';
$lang['sampleDelai']		= 'Cache time delay for sample (sec)';
$lang['sampleDelai']		= 'Cache time delay for list (sec)';
?>
